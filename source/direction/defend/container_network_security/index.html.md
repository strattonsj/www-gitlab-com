---
layout: markdown_page
title: "Category Direction - Container Network Security"
---

- TOC
{:toc}

## Description
Containers are a critical part of modern applications. They contain the business logic and data needed to operate and are frequently spun up and down based on load, costs, and other factors. Because containers are such a critical part of apps, they need to be defended from attacks and inappropriate use. This is even more important when one app is on the same cluster as another app.

### Goal
GitLab's goal with container network security is that any app can be run on a cluster with any other app and be confident that it can properly identify and stop any unintended use or traffic.
Our goal is also to be able to detect rogue containers and other resources inappropriately added to clusters so that they can either be deleted or blacklisted.
Finally, our goal is to be able to do everything and provide informative results outwards as either Issues or Vulnerabilities, so the security team and others in your organization can act to remediate existing and prevent future issues.

### Roadmap
[Planned to Minimal](https://gitlab.com/groups/gitlab-org/-/epics/1821)

## What's Next & Why
We will start by [adding NetworkPolicy objects to Kubernetes clusters](https://gitlab.com/gitlab-org/gitlab/issues/14010) as the first step in this category. This provides a powerful baseline for which more advanced networking controls can be added to.

## Competitive Landscape
TODO

## Analyst Landscape
TODO

## Top Customer Success/Sales Issue(s)
TODO

## Top Customer Issue(s)
The category is very new, so we still need to engage customers and get feedback about their interests and priorities in this area.

## Top Vision Item(s)
TODO
